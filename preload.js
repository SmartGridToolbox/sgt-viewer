// Copyright 2020 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// sgt-viewer was written by Dan Gordon.

"use strict";

const url = 'http://localhost:34568';

// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
    const replaceText = (selector, text) => {
        const element = document.getElementById(selector);
        if (element) element.innerText = text;
    };

    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type]);
    }
})

const electron = require('electron');
const {contextBridge, ipcRenderer} = electron;

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object

// KLUDGE: set the zoom factor here.
process.once('loaded', () => {
    electron.webFrame.setZoomFactor(1.0);
});

var eNetworkViewer = null;
window.onload = () => {
    const viewer = require('e-network-viewer');
    const networkDiv = document.getElementById('network'); 
    const propertiesDiv = document.getElementById('properties'); 
    eNetworkViewer = viewer.ENetworkViewer(networkDiv, propertiesDiv);
    eNetworkViewer.setPropertiesUpdate(propertiesUpdate);
}

async function openFile(filePath) {
    const fs = require('fs');
    const raw = await fs.readFileSync(filePath, 'utf-8');
    try {
        let resp = await fetch(url + '?action=load_network', {method: 'POST', body: raw, mode: 'no-cors'});
    } catch(err) {
        alert('Failed to load new network');
    }
    const netwProm = fetch(url + '?view=network').then(resp => resp.json());

    let netwJsn;
    try {
        netwJsn = await netwProm;
    } catch(err) {
        alert('Failed to obtain network. ' + err);
    }

    eNetworkViewer.loadNetwork(netwJsn);
};

async function propertiesUpdate(compId, compType) {
    let resp = null;
    try {
        resp = await fetch(url + '?view=' + compType + '&id=' + compId, {method: 'GET'}).then(resp => resp.json());
    } catch(err) {
        alert('Failed to solve network. ' + err);
    }
    return resp;
}

ipcRenderer.on('open-file', (event, filePath) => {
    openFile(filePath);
});

contextBridge.exposeInMainWorld('api', {
    eNetworkViewer: () => {return eNetworkViewer;},
});

// vim: ts=4 sw=4:
